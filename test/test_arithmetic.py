"""Tests basic_math.py."""

import unittest
from basic_math.arithmetic import Arithmetic

class TestArithmetic(unittest.TestCase):
    """Tests basic_math.Arithmetic."""

    def test_multiplication(self) -> None:
        """Tests basic_math.Arithmetic.multiply_float."""
        actual = Arithmetic.multiply_float(3, 4)
        expected = 12.
        self.assertEqual(actual, expected)

    def test_addition(self) -> None:
        """Tests basic_math.Arithmetic.add_float."""
        actual = Arithmetic.add_float(3, 4)
        expected = 7.
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
