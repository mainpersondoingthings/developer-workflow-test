from setuptools import setup

setup(
    name='basic_math',
    version='0.0.1',
    description='My private package from private github repo',
    url='git@github.com:mainpersondoingthings/developer-workflow-test.git',
    author='Luke Hartman',
    author_email='luke.hartman@tmlt.io',
    license='unlicense',
    packages=['basic_math'],
    zip_safe=False
)
