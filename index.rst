Welcome to Sphinx Example Project's documentation!
==================================================

Contents:

.. toctree::
   :maxdepth: 2

   modules.rst

`coverage <coverage/index.html>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
