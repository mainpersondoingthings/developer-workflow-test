"""Amazing math module!

This module is the home of Arithmetic, which is filled with useful stuff. Since
this module contains Arithmetic, we know that it also has lots of useful stuff
in it."""

class Arithmetic():
    """Lots of useful arithmatic operators!

    This class is filled with lots of very useful stuff!"""

    @staticmethod
    def multiply_float(arg1: float, arg2: float) -> float:
        """Returns arg1 * arg2.

        Args:
            arg1: The first number to multiply.
            arg2: The second number to multiply.
        """
        return arg1 * arg2

    @staticmethod
    def add_float(arg1: float, arg2: float) -> float:
        """Returns arg1 + arg2.

        Args:
            arg1: The first number to multiply.
            arg2: The second number to multiply.
        """
        return arg1 + arg2
